package qld.mock.vaccination.dto;

import java.time.LocalDate;

public class NewsDto {

	private String title;
	
	private String preview;
	
	private String content;
	

	public NewsDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NewsDto(String title, String preview, String content) {
		super();
		this.title = title;
		this.preview = preview;
		this.content = content;
		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPreview() {
		return preview;
	}

	public void setPreview(String preview) {
		this.preview = preview;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}


	
}
